//setTimeout() - позволяет вызвать функцию один раз через определённый интервал времени.
//setInterval - позволяет вызывать функцию регулярно, повторяя вызов через определённый интервал времени.
//delay - это задержка перед запуском в миллисекундах (1000 мс = 1 с). Значение по умолчанию – 0.
//Функция clearInterval() отменяет многократные повторения действий, установленные вызовом функции setInterval().




const imgs = document.querySelectorAll('.image-to-show');
let myCount = 0;
let intervalSlide = setInterval(nextSlide, 3000);
let stopBtn = document.querySelector('#stopBtn');
let playBtn = document.querySelector('#playBtn');


function nextSlide(){
	imgs[myCount].className = 'image-to-show';
	myCount = (myCount + 1) % imgs.length; // 
	imgs[myCount].className = 'image-to-show active';
}
stopBtn.addEventListener('click', function(){
	if(intervalSlide){
		clearInterval(intervalSlide);
		intervalSlide = null;
	}else if(!intervalSlide){
		return false
	}
});

playBtn.addEventListener('click', function(){
	if (intervalSlide) {
		return false;
	}else if(!intervalSlide){
		intervalSlide = setInterval(nextSlide, 3000);
	}
})




















//function QQQ(){
//	myVar = setInterval(() => {
//		imgs.forEach(item =>{
//			if(item.classList.contains('active')){
//				item.classList.remove('active');
//			}else{
//				item.classList.add('active');
//			}
//		})
//	}, 2000);
//}
//QQQ()




//if(el.classList.contains('active')){
//	el.style.display = 'block';
//}else {
//	el.style.display = 'none';
//}


//function qqq(){
//	imgs.forEach(item =>{
//		myVar = setInterval(() =>{
//			item.style.opacity = '1';
//		},100);
//	})
//}
////qqq()



//function clearTimeout(){
//	stopBtn.addEventListener('click', () =>{
//		clearTimeout(myVar, 2000);
//	})
//}


