//Функции нужны для того чтобы не повторять один и тот же код во многих местах и в то же время вызвать его в любой части программы.
//Аргументы передаются в параметры функции, которые будут использоватся в локальных переменных. Если не передавать аргументы то функция выведет undefined

let numFirst;
let numSecond;
	do {
		numFirst = +prompt('First number');
	}while(Number.isNaN(numFirst) || numFirst == "" );
	do {
		numSecond = +prompt('Second number');
	}while(Number.isNaN(numSecond) || numSecond == "");
	
	let operator = prompt('Operator?');
	function calcNumbers(a, b) {
		switch (operator){
			case '+':
				return a + b;
			case '-':
				return a - b;
			case '*':
				return a * b;
			case '/':
				return a / b;
	}
}
console.log(`Result: ${calcNumbers(numFirst,numSecond)}`);
