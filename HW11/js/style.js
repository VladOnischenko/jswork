//некорректная работа keyup keydown c input

const allBtn = document.querySelectorAll('.btn');

function blackColor(){
	allBtn.forEach(element => {
		element.style.background = 'black';
	})
}

document.addEventListener('keyup', (event)=>{
	for(let element of allBtn){
		if(event.code === element.dataset.name){
			blackColor();
			element.style.background = 'blue';
		}
	}
})
