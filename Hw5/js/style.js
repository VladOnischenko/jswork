//Это специальные символы, которые имеют особое значение в регулярном выражении. Они используются для более сложных поисковых конструкций. 

class CreateNewUser {
	constructor() {
		this.firstName = prompt("Enter  first name!");
		this.lastName = prompt("Enter last name!");
		this.birthday = prompt("Enter Date of Birth", "dd.mm.yyyy");
	};
	getLogin() {
		return `${this.firstName[0].toLowerCase()}${this.lastName.toLowerCase()}`;
	};
	getAge() {
		const today = new Date();
		const userBirthday = Date.parse(`${this.birthday.slice(6)}-${this.birthday.slice(3, 5)}-${this.birthday.slice(0, 2)}`);
		const age = ((today - userBirthday) / (1000 * 60 * 60 * 24 * 30 * 12)).toFixed(0);
		return `Вам ${age}`;
	};
	getPassword() {
		return `${this.firstName[0].toUpperCase()}${this.lastName.toLocaleLowerCase()}${this.birthday.slice(-4)}`
	};
};

const newUser = new CreateNewUser();
console.log(newUser.getAge());
console.log(`Ваш логин: ${newUser.getLogin()}`);
console.log(`Ваш пароль: ${newUser.getPassword()}`);










