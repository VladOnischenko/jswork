// forEach используется для перебора массива.

let arr = ['hello', 'world', 23, '23', null];
let filterBy = (item, type) => {
	return item.filter(function (value) {
		return typeof value !== type;
	})
}
console.log(filterBy(arr, 'string'));