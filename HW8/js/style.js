//Oбработчик событий - это сигнал от браузера о том, что что-то произошло, все DOM-узлы подают такие сигналы(события мыши, клавиатурные события и т.д)

const input = document.createElement('input')
document.body.append(input)
input.addEventListener('focus', event => {
    input.style.border ='2px solid green';
})
input.addEventListener('blur', event => {
    input.style.border ='';
    const message = document.createElement('span')
    if (input.value < 0) {
        input.style.border ='2px solid red';
        const alertMessage = document.createElement('span')
        alertMessage.innerText = 'Please enter correct price';
        alertMessage.style.display = 'flex'
        document.body.append(alertMessage)
    }else {
    message.innerText = `Price: ${input.value}`
    message.style.display = 'flex'
    input.style.color = 'green'
    document.body.prepend(message)
    const spanRemover = document.createElement('span')
    spanRemover.innerText = 'X'
    spanRemover.style.border = '1px solid blue'
    message.append(spanRemover)
    spanRemover.addEventListener('click', event => {
        message.remove()
        spanRemover.remove()
        input.value = ''
    })
    }
})
