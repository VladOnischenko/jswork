//Методы JavaScript это действия, которые можно выполнить с объектами. Ключевое слово this ссылается на "владельца" функции.
class CreateNewUser {
	constructor(nameF, nameL) {
		this.firstName = nameF;
		this.lastName = nameL;
	}
	getLogin() {
		return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
	}
}
const newUser = new CreateNewUser(prompt("Enter first name!"), prompt("Enter last name!"));
console.log(newUser.getLogin()); 




