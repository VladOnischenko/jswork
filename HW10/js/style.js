const btn = document.querySelector('button');
const input1 = document.querySelector('.input1')
const input2 = document.querySelector('.input2');
const infoText = document.createElement('div')
const icon1= document.querySelector('#icon1');
const icon2= document.querySelector('#icon2');

infoText.textContent = 'Нужно ввести одинаковые значения';
infoText.style.color = 'red';
infoText.style.width = '300px';
infoText.style.marginBottom = '10px';

btn.addEventListener('click', () =>{
	if (input1.value !== input2.value){
		input2.after(infoText);
	}else if(input1.value == '' || input2.value == ''){
		input2.after(infoText);
	}else {
		alert('You are welcome')
		input1.value = '';
		input2.value = '';
		infoText.remove()
	}
})
icon1.addEventListener('click', () => {
	if(input1.getAttribute('type') == 'password'){
		icon1.className = 'fas fa-eye-slash icon-password';
		input1.setAttribute('type', 'text')
	}else {
		icon1.className = 'fas fa-eye icon-password';
		input1.setAttribute('type', 'password')
	}
	return false
});

icon2.addEventListener('click', () => {
	if(input2.getAttribute('type') == 'password'){
		icon2.className = 'fas fa-eye-slash icon-password';
		input2.setAttribute('type', 'text')
	}else {
		icon2.className = 'fas fa-eye icon-password';
		input2.setAttribute('type', 'password')
	}
	return false
});




