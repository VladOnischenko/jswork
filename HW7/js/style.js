//Document Object Model (DOM) - это объектная модель документа, каждый HTML-тег является объектом и все эти объекты доступны при помощи JavaScript, мы можем использовать их для изменения страницы.

let arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
let ul = document.createElement("ul");
document.body.append(ul);

let list = arr.map(function(item) {
	item = `<li>${item}</li>`;
	return item;
})
ul.insertAdjacentHTML('beforeend',list.join(' '))


//function listItems(arr) {
	//for( let i = 0;i < arr.length; i++ ) {
	//	let li = document.createElement("li"); // один из способов 
	//	li.innerText = arr[i];
	//	ul.append(li);
	//}
//}
//listItems(arr);

