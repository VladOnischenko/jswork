//var - объявляет переменную глобально или локально во всей функции, независимо от области блока. Таким образом могут появится трудности с переменными var и это считается устаревшим методом.
//let - позволяет объявить локальную переменную с областью видимости, ограниченной текущим блоком кода 
//const - Это объявление создаёт константу, чья область действия может быть как глобальной, так и локальной внутри блока, в котором она объявлена.


let userName = null;
let userAge = null;

do {
	userName = prompt('Your name?');
}while (userName === '' || !isNaN(userName));
do {
	userAge = +prompt('Your age?')
}while (Number.isNaN(userAge));
	if (userAge < 18) {
		alert('You are not allowed to visit this website');
	}else if (userAge <= 23) {
		let userAccess = confirm('Are you sure you want to continue?');
		if (userAccess) {
			alert(`Welcome: ${userName}`)
		}else {
			alert ('You are not allowed to visit this website');
		}
	}if (userAge > 23) {
		alert(`Welcome: ${userName}`)
	}
